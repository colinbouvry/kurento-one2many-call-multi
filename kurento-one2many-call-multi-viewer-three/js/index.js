/*
 * (C) Copyright 2014-2015 Kurento (http://kurento.org/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


var ws;
var video;
var webRtcPeer;

window.onload = function() {
	console = new Console();
	video = document.getElementById('video');

	document.getElementById('viewer').addEventListener('click', function() { viewer(); } );
	document.getElementById('terminate').addEventListener('click', function() { stop(); } );

	// update loop
	var mainLoopId = setInterval(function(){
		if (!webRtcPeer) {
			updateViewer();
		}
	}, 1000);

	newWebsocket();
}

window.onbeforeunload = function() {
	ws.close();
}

// kurento viewer function compatible kurento-one2many-call-multi-admin
function newWebsocket() {
	ws = new WebSocket('ws://192.168.1.12:8443/one2many');
	lasttime = getCurrentTime();

	ws.onmessage = function(message) {
		var parsedMessage = JSON.parse(message.data);
		console.info('Received message: ' + message.data);
	
		switch (parsedMessage.id) {
		case 'viewerResponse':
			viewerResponse(parsedMessage);
			break;
		case 'stopCommunication':
			dispose();
			break;
		case 'iceCandidate':
			webRtcPeer.addIceCandidate(parsedMessage.candidate)
			break;
		default:
			console.error('Unrecognized message', parsedMessage);
		}
	}

	ws.onopen = function(e) {
		console.log('Websocket onopen !');
		viewer();
	};
	
	ws.onclose = function(e) {
		console.log('Websocket onclose !');
		dispose();
	};
	ws.onerror = function (e) {
		console.log('Websocket onError !');
		dispose();
	};
}

function updateViewer()
{
	// if not webRtcPeer call viewer every second
	var elapsedtime = getCurrentTime() - lasttime;
	if( elapsedtime > 1000) {
		lasttime = getCurrentTime();
		if(ws.readyState === ws.CLOSED){
			console.log('ws state CLOSED' );
			dispose();
			newWebsocket();
		}
		else if(ws.readyState === ws.CLOSING){
			console.log('ws state CLOSING' );
			dispose();
			newWebsocket();
		}
		else if(ws.readyState === ws.OPEN){
			console.log('ws state OPEN' );
		}
		else if(ws.readyState === ws.CONNECTING){
			console.log('ws state CONNECTING' );
		}
		if (!webRtcPeer) {
			console.log('Call viewer');
			viewer();
		}
	}
}

function getCurrentTime() {
	var my_current_timestamp;
	my_current_timestamp = new Date();      //stamp current date & time
	return my_current_timestamp.getTime();
}

function viewerResponse(message) {
	if (message.response != 'accepted') {
		var errorMsg = message.message ? message.message : 'Unknow error';
		console.log('Call not accepted for the following reason: ' + errorMsg);
		dispose();
	} else {
		webRtcPeer.processAnswer(message.sdpAnswer);
	}
}

function viewer() {
	if (!webRtcPeer && ws.readyState === ws.OPEN) {
		showSpinner(video);

		var options = {
			remoteVideo: video,
			onicecandidate : onIceCandidate
		}

		webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function(error) {
			if(error) return onError(error);

			this.generateOffer(onOfferViewer);
		});
	}
}

function onOfferViewer(error, offerSdp) {
	if (error) return onError(error)

	var message = {
		id : 'viewer',
		sdpOffer : offerSdp
	}
	sendMessage(message);
}

function onIceCandidate(candidate) {
	console.log('Local candidate' + JSON.stringify(candidate));

	var message = {
		id : 'onIceCandidate',
		candidate : candidate
	}
	sendMessage(message);
}

function stop() {
	if (webRtcPeer) {
		var message = {
				id : 'stop'
		}
		sendMessage(message);
		dispose();
	}
}

function dispose() {
	if (webRtcPeer) {
		webRtcPeer.dispose();
		webRtcPeer = null;
	}
	hideSpinner(video);
}

function sendMessage(message) {
	var jsonMessage = JSON.stringify(message);
	console.log('Senging message: ' + jsonMessage);

	// 4 values : CONNECTING OPEN CLOSING or CLOSED
	if(ws.readyState === ws.OPEN){
		ws.send(jsonMessage);
	}
}

function showSpinner() {

}

function hideSpinner() {

}

function onError(error) {
	console.error(error);
}

/**
 * Lightbox utility (to display media pipeline image in a modal dialog)
 */
$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
	event.preventDefault();
	$(this).ekkoLightbox();
});
