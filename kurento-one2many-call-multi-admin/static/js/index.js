/*
 * (C) Copyright 2014-2015 Kurento (http://kurento.org/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
var address;
var ws;
var webRtcPeer;
var audioSelect;
var videoSelect;

//http://papermashup.com/read-url-get-variables-withjavascript/
function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	vars[key] = value;
	});
	return vars;
}

window.onload = function() {

	console = new Console();
	video = document.getElementById('video');

	document.getElementById('call').addEventListener('click', function() { presenter(); } );
	document.getElementById('call_player').addEventListener('click', function() { presenterPlayer(); } );
	document.getElementById('viewer').addEventListener('click', function() { viewer(); } );
	document.getElementById('terminate').addEventListener('click', function() { stop(); } );

	// list media
	audioSelect = document.querySelector('select#audioSource');
	videoSelect = document.querySelector('select#videoSource');
	navigator.mediaDevices.enumerateDevices().then(gotDevices).then(getStream).catch(handleError);
	audioSelect.onchange = getStream;
	videoSelect.onchange = getStream;

	address = getUrlVars()["address"];
	if(address == undefined)
		address = '/one2many';
	console.log('Address : ', address);

	newWebsocket();
}

// List media

function getStream() {
	if (window.stream) {
	window.stream.getTracks().forEach(function(track) {
		track.stop();
	});
	}

	var constraints = {
	audio: {
		deviceId: {exact: audioSelect.value}
	},
	video: {
		deviceId: {exact: videoSelect.value}
	}
	};

	navigator.mediaDevices.getUserMedia(constraints).
	then(gotStream).catch(handleError);
}

function gotDevices(deviceInfos) {
	for (var i = 0; i !== deviceInfos.length; ++i) {
	var deviceInfo = deviceInfos[i];
	var option = document.createElement('option');
	option.value = deviceInfo.deviceId;
	if (deviceInfo.kind === 'audioinput') {
		option.text = deviceInfo.label ||
		'microphone ' + (audioSelect.length + 1);
		audioSelect.appendChild(option);
	} else if (deviceInfo.kind === 'videoinput') {
		option.text = deviceInfo.label || 'camera ' +
		(videoSelect.length + 1);
		videoSelect.appendChild(option);
	} else {
		console.log('Found one other kind of source/device: ', deviceInfo);
	}
	}
}

function gotStream(stream) {
	window.stream = stream; // make stream available to console
	//video.srcObject = stream;
}

function handleError(error) {
	console.log('Error: ', error);
}

window.onbeforeunload = function() {
	ws.close();
}

function newWebsocket() {
	ws = new WebSocket('wss://' + location.host + address);

	ws.onmessage = function(message) {
		var parsedMessage = JSON.parse(message.data);
		console.info('Received message: ' + message.data);

		switch (parsedMessage.id) {
		case 'presenterResponse':
			presenterResponse(parsedMessage);
			break;
		case 'viewerResponse':
			viewerResponse(parsedMessage);
			break;
		case 'stopCommunication':
			dispose();
			break;
		case 'iceCandidate':
			webRtcPeer.addIceCandidate(parsedMessage.candidate)
			break;
		default:
			console.error('Unrecognized message', parsedMessage);
		}
	}

	ws.onopen = function(e) {
		console.log('Websocket onopen !');
	};

	ws.onclose = function(e) {
		console.log('Websocket onclose !');
		stop(); // TO DO TEST
	};
	ws.onerror = function (e) {
		console.log('Websocket onError !');
		stop(); // TO DO TEST
	};
}

function presenterResponse(message) {
	if (message.response != 'accepted') {
		var errorMsg = message.message ? message.message : 'Unknow error';
		console.warn('Call not accepted for the following reason: ' + errorMsg);
		dispose();
	} else {
		webRtcPeer.processAnswer(message.sdpAnswer);
	}
}

function viewerResponse(message) {
	if (message.response != 'accepted') {
		var errorMsg = message.message ? message.message : 'Unknow error';
		console.warn('Call not accepted for the following reason: ' + errorMsg);
		dispose();
	} else {
		webRtcPeer.processAnswer(message.sdpAnswer);
	}
}

function presenter() {
	if (!webRtcPeer) {
		showSpinner(video);

		// directshow selection 
		var constraints = {
			audio: false,
			video: {
			//width : { exact : 1280},
			//height : { exact :  720 },
			//framerate : { exact : 30 },
			deviceId : { exact: videoSelect.value }
			}
		}

		var options = {
		localVideo : video,
		onicecandidate : onIceCandidate,
		mediaConstraints : constraints,
		sendSource : 'webcam'
		}
		//var options = {
		//	localVideo: video,
		//	onicecandidate : onIceCandidate
	    //}

		webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, function(error) {
			if(error) return onError(error);

			this.generateOffer(onOfferPresenter);
		});
	}
}

function onOfferPresenter(error, offerSdp) {
    if (error) return onError(error);

	var message = {
		id : 'presenter',
		sdpOffer : offerSdp
	};
	sendMessage(message);
}

function presenterPlayer() {
	if (!webRtcPeer) {
		showSpinner(video);

		// Video and audio by default
		var userMediaConstraints = {
			audio : false,
			video : true
		}

		var options = {
			remoteVideo : video,
			mediaConstraints : userMediaConstraints,
			onicecandidate : onIceCandidate
		}

		console.info('User media constraints' + userMediaConstraints);

		webRtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options,
			function(error) {
				if(error) return onError(error);
				this.generateOffer(onOfferPresenterPlayer);
			});
	}
}

function onOfferPresenterPlayer(error, offerSdp) {
    if (error) return onError(error);

	var message = {
		id : 'presenter_player',
		sdpOffer : offerSdp,
		videourl : document.getElementById('videourl').value
	};
	sendMessage(message);
}

function viewer() {
	if (!webRtcPeer) {
		showSpinner(video);

		var options = {
			remoteVideo: video,
			onicecandidate : onIceCandidate
		}

		webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function(error) {
			if(error) return onError(error);

			this.generateOffer(onOfferViewer);
		});
	}
}

function onOfferViewer(error, offerSdp) {
	if (error) return onError(error)

	var message = {
		id : 'viewer',
		sdpOffer : offerSdp
	}
	sendMessage(message);
}

function onIceCandidate(candidate) {
	   console.log('Local candidate' + JSON.stringify(candidate));

	   var message = {
	      id : 'onIceCandidate',
	      candidate : candidate
	   }
	   sendMessage(message);
}

function stop() {
	if (webRtcPeer) {
		var message = {
				id : 'stop'
		}
		sendMessage(message);
		dispose();
	}
}

function dispose() {
	if (webRtcPeer) {
		webRtcPeer.dispose();
		webRtcPeer = null;
	}
	hideSpinner(video);
}

function sendMessage(message) {
	var jsonMessage = JSON.stringify(message);
	console.log('Senging message: ' + jsonMessage);
	ws.send(jsonMessage);
}

function showSpinner() {
	for (var i = 0; i < arguments.length; i++) {
		arguments[i].poster = './img/transparent-1px.png';
		arguments[i].style.background = 'center transparent url("./img/spinner.gif") no-repeat';
	}
}

function hideSpinner() {
	for (var i = 0; i < arguments.length; i++) {
		arguments[i].src = '';
		arguments[i].poster = './img/webrtc.png';
		arguments[i].style.background = '';
	}
}

function onError(error) {
    console.error(error);
}

/**
 * Lightbox utility (to display media pipeline image in a modal dialog)
 */
$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
	event.preventDefault();
	$(this).ekkoLightbox();
});
